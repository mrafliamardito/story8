from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from django.apps import apps
from .apps import Story8KuttttConfig

# Create your tests here.
class Story8(TestCase):

    def test_page(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'story8.html')
    
    def test_json_url(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code,200)
    
    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("books",html_response)

    def test_apps(self):
        self.assertEqual(Story8KuttttConfig.name, 'story8kutttt')
        self.assertEqual(apps.get_app_config('story8kutttt').name, 'story8kutttt') 